/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2016 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

using System.Collections.Generic;
using System.Linq;
using Bomberman.Api;

namespace Demo
{
    /// <summary>
    /// This is BombermanAI client demo.
    /// </summary>
    internal class YourSolver : AbstractSolver
    {
        public YourSolver(string name, string server)
            : base(name, server)
        {            
        }

        /// <summary>
        /// Calls each move to make decision what to do (next move)
        /// </summary>
        protected override string Get(Board board)
        {
            var path = board.GetPathToClosestEnemy();
            if (path.Count > 0)
            {
                //if (!board.GetFutureBlasts().Contains(board.GetBomberman()))
                //{
                //    if (board.GetFutureBlasts().Contains(path[0]))
                //    {
                //        return Direction.Stop.ToString();
                //    }
                //}

                if (path.Count > 4 && board.GetBlastArea(board.GetBomberman())
                        .Any(x => board.GetAt(x) == Element.MEAT_CHOPPER || board.GetAt(x) == Element.DESTROYABLE_WALL))
                {
                    if (!board.GetBombs().Contains(board.MyBombLocation))
                        board.MyBombLocation = board.GetBomberman();
                    return Direction.Act + ", " + board.GetDirectionTo(path[0]).ToString();
                }
                else if (path.Count == 1)
                {
                    if (!board.GetBombs().Contains(board.MyBombLocation))
                        board.MyBombLocation = path[0];
                    return board.GetDirectionTo(path[0]).ToString() + ", " + Direction.Act;
                }
                return board.GetDirectionTo(path[0]).ToString();
            }
            else
            {
                if (board.GetFutureBlasts().Contains(board.GetBomberman()))
                {
                    path = board.GetPathToSafety();
                    return board.GetDirectionTo(path[0]).ToString();
                }
            }
            if (!board.GetBombs().Contains(board.MyBombLocation))
                board.MyBombLocation = board.GetBomberman();
            return Direction.Act.ToString();
        }

        
    }
}