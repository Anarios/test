/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2016 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bomberman.Api
{
    public class Board
    {

        private String BoardString;
        private LengthToXY LengthXY;

        public Board(String boardString)
        {
            BoardString = boardString.Replace("\n", "");
            LengthXY = new LengthToXY(Size);
        }

        /// <summary>
        /// GameBoard size (actual board size is Size x Size cells)
        /// </summary>
        public int Size
        {
            get
            {
                return (int)Math.Sqrt(BoardString.Length);
            }
        }

        public Point GetBomberman()
        {
            return Get(Element.BOMBERMAN)
                    .Concat(Get(Element.BOMB_BOMBERMAN))
                    .Concat(Get(Element.DEAD_BOMBERMAN))
                    .Single();
        }

        public List<Point> GetOtherBombermans()
        {
            return Get(Element.OTHER_BOMBERMAN)
                .Concat(Get(Element.OTHER_BOMB_BOMBERMAN))
                .Concat(Get(Element.OTHER_DEAD_BOMBERMAN))
                .ToList();
        }

        public bool isMyBombermanDead
        {
            get
            {
                return BoardString.Contains((char)Element.DEAD_BOMBERMAN);
            }
        }

        public Element GetAt(Point point)
        {
            if (point.IsOutOf(Size))
            {
                return Element.WALL;
            }
            return (Element)BoardString[LengthXY.GetLength(point.X, point.Y)];
        }

        public bool IsAt(Point point, Element element)
        {
            if (point.IsOutOf(Size))
            {
                return false;
            }

            return GetAt(point) == element;
        }

        public string BoardAsAtring()
        {
            string result = "";
            for (int i = 0; i < Size; i++)
            {
                StringBuilder currentLine = new StringBuilder(BoardString.Substring(i * Size, Size));
                foreach (var point in Path.Where(x => x.Y == Size - i - 1))
                {
                    currentLine[point.X] = '*';
                }

                result += currentLine;
                result += "\n";
            }
            return result;
        }

        /// <summary>
        /// gets board view as string
        /// </summary>
        public string ToString()
        {
           return string.Format("{0}\n" +
                    "Bomberman at: {1}\n" +
                    "Other bombermans at: {2}\n" +
                    "Meat choppers at: {3}\n" +
                    "Destroy walls at: {4}\n" +
                    "Bombs at: {5}\n" +
                    "Blasts: {6}\n" +
                    "Expected blasts at: {7}",
                    BoardAsAtring(),
                    GetBomberman(),
                    ListToString(GetOtherBombermans()),
                    ListToString(GetMeatChoppers()),
                    ListToString(GetDestroyableWalls()),
                    ListToString(GetBombs()),
                    ListToString(GetBlasts()),
                    ListToString(GetFutureBlasts()));
        }

        private string ListToString(List<Point> list)
        {
            return string.Join(",", list.ToArray());
        }

        public List<Point> GetBarrier()
        {
            return GetMeatChoppers()
                .Concat(GetWalls())
                .Concat(GetBombs())
                .Concat(GetDestroyableWalls())
                .Concat(GetOtherBombermans())
                .Distinct()
                .ToList();
        }

        public List<Point> GetMeatChoppers()
        {
            return Get(Element.MEAT_CHOPPER);
        }

        public List<Point> Get(Element element)
        {
            List<Point> result = new List<Point>();

            for (int i = 0; i < Size * Size; i++)
            {
                Point pt = LengthXY.GetXY(i);

                if (IsAt(pt, element))
                {
                    result.Add(pt);
                }
            }

            return result;
        }

        public List<Point> GetWalls()
        {
            return Get(Element.WALL);
        }

        public List<Point> GetDestroyableWalls()
        {
            return Get(Element.DESTROYABLE_WALL);
        }

        public List<Point> GetBombs()
        {
            return Get(Element.BOMB_TIMER_1)
                .Concat(Get(Element.BOMB_TIMER_2))
                .Concat(Get(Element.BOMB_TIMER_3))
                .Concat(Get(Element.BOMB_TIMER_4))
                .Concat(Get(Element.BOMB_TIMER_5))
                .Concat(Get(Element.BOMB_BOMBERMAN))
                .Concat(Get(Element.OTHER_BOMB_BOMBERMAN))
                .ToList();
        }

        public List<Point> GetBlasts()
        {
            return Get(Element.BOOM);
        }

        public List<Point> GetBlastArea(Point p)
        {
            var result = new List<Point>();
            result.Add(p);
            result.AddRange(TracePathToDirection(p.ShiftTop));
            result.AddRange(TracePathToDirection(p.ShiftRight));
            result.AddRange(TracePathToDirection(p.ShiftBottom));
            result.AddRange(TracePathToDirection(p.ShiftLeft));
            return result;
        }

        private List<Point> TracePathToDirection(Func<int, Point> shiftFunc)
        {
            var result = new List<Point>();
            for (int i = 1; i <= 3 && GetAt(shiftFunc(i)) != Element.WALL; i++)
            {
                result.Add(shiftFunc(i));
            }
            return result;
        }

        public List<Point> GetFutureBlasts()
        {
            var bombs = GetBombs();
            var result = new List<Point>();
            foreach (var bomb in bombs)
            {
                result.AddRange(GetBlastArea(bomb));
               
            }

            return result.Where(blast => !blast.IsOutOf(Size) && !GetWalls().Contains(blast)).Distinct().ToList();
        }

        public bool IsAnyOfAt(Point point, params Element[] elements)
        {
            return elements.Any(elem => IsAt(point, elem));
        }

        public bool IsNear(Point point, Element element)
        {
            if (point.IsOutOf(Size))
                return false;

            return IsAt(point.ShiftLeft(),   element) ||
                   IsAt(point.ShiftRight(),  element) ||
                   IsAt(point.ShiftTop(),    element) ||
                   IsAt(point.ShiftBottom(), element);
        }

        public bool IsBarrierAt(Point point)
        {
            return GetBarrier().Contains(point);
        }

        public int CountNear(Point point, Element element)
        {
            if (point.IsOutOf(Size))
                return 0;

            int count = 0;
            if (IsAt(point.ShiftLeft(),   element)) count++;
            if (IsAt(point.ShiftRight(),  element)) count++;
            if (IsAt(point.ShiftTop(),    element)) count++;
            if (IsAt(point.ShiftBottom(), element)) count++;
            return count;
        }

        public List<Point> GetNeighbourCells(Point point, bool includeDangerousPath = false)
        {
            var result = new List<Point>();
            if(IsAnyOfAt(point.ShiftTop(), Element.Space, Element.BOOM))
                result.Add(point.ShiftTop());
            if (IsAnyOfAt(point.ShiftRight(), Element.Space, Element.BOOM))
                result.Add(point.ShiftRight());
            if (IsAnyOfAt(point.ShiftBottom(), Element.Space, Element.BOOM))
                result.Add(point.ShiftBottom());
            if (IsAnyOfAt(point.ShiftLeft(), Element.Space, Element.BOOM))
                result.Add(point.ShiftLeft());

            if(!includeDangerousPath)
                result = result.Except(GetDontStepCells()).ToList();
            return result.OrderBy(a => GetFutureBlasts().Contains(a)).ThenBy(x => Guid.NewGuid()).ToList();
        }

        private List<Point> GetDontStepCells()
        {
            return GetBlastArea(Get(Element.BOMB_TIMER_1).Concat(Get(Element.BOMB_TIMER_2)));
        }

        private List<Point> GetBlastArea(IEnumerable<Point> points)
        {
            var result = new List<Point>();
            foreach (var point in points)
            {
                result.AddRange(GetBlastArea(point));
            }
            return result;
        }

        public List<Point> GetPathToClosestEnemy()
        {
            var previous = new Dictionary<Point, Point>();

            var queue = new Queue<Point>();
            queue.Enqueue(GetBomberman());
            Point target = new Point();

            while (queue.Count > 0)
            {
                var vertex = queue.Dequeue();
                //if (IsNear(vertex, Element.OTHER_BOMBERMAN))
                //if(!GetFutureBlasts().Contains(vertex))
                var peopleToBomb = GetBlastArea(vertex)
                    .Where(x => IsAt(x, Element.OTHER_BOMBERMAN) || IsAt(x, Element.OTHER_BOMB_BOMBERMAN)).ToList();
                if (peopleToBomb.Any(x => !IsAnyOfAt(MyBombLocation, Element.BOMB_TIMER_1, Element.BOMB_TIMER_2, Element.BOMB_TIMER_3, Element.BOMB_TIMER_4, Element.BOMB_TIMER_5, Element.BOMB_BOMBERMAN)
                                          || !GetBlastArea(MyBombLocation).Contains(x)))
                {
                  
                    target = vertex;
                    goto outOfWhile;
                    
                }

                foreach (var neighbor in GetNeighbourCells(vertex))
                {
                    if (previous.ContainsKey(neighbor))
                        continue;

                    previous[neighbor] = vertex;

                    queue.Enqueue(neighbor);
                }
            }
        outOfWhile:
            var result = new List<Point>();
            while (previous.ContainsKey(target))
            {
                result.Add(target);
                target = previous[target];
            }
            result.Reverse();
            Path = result;
            return result;
        }

        public List<Point> Path { get; set; } = new List<Point>();

        public Direction GetDirectionTo(Point point)
        {
            var me = GetBomberman();
            if (point.X > me.X)
                return Direction.Right;
            if (point.X < me.X)
                return Direction.Left;
            if (point.Y > me.Y)
                return Direction.Up;
            if (point.Y < me.Y)
                return Direction.Down;

            return Direction.Stop;
        }

        public List<Point> GetPathToSafety()
        {
            var previous = new Dictionary<Point, Point>();

            var queue = new Queue<Point>();
            queue.Enqueue(GetBomberman());
            Point target = new Point();

            while (queue.Count > 0)
            {
                var vertex = queue.Dequeue();
                //if (IsNear(vertex, Element.OTHER_BOMBERMAN))
                if(!GetFutureBlasts().Contains(vertex))
                //var peopleToBomb = GetBlastArea(vertex)
                //    .Where(x => IsAt(x, Element.OTHER_BOMBERMAN) || IsAt(x, Element.OTHER_BOMB_BOMBERMAN)).ToList();
                //if (peopleToBomb.Any(x => !GetFutureBlasts().Contains(x)))
                {

                    target = vertex;
                    goto outOfWhile;

                }

                foreach (var neighbor in GetNeighbourCells(vertex, true))
                {
                    if (previous.ContainsKey(neighbor))
                        continue;

                    previous[neighbor] = vertex;

                    queue.Enqueue(neighbor);
                }
            }
            outOfWhile:
            var result = new List<Point>();
            while (previous.ContainsKey(target))
            {
                result.Add(target);
                target = previous[target];
            }
            result.Reverse();
            Path = result;
            return result;
        }

        public Point MyBombLocation;
    }
}